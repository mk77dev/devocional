﻿namespace LeituraBibliaCalendario
{
    using Ical.Net;
    using Ical.Net.CalendarComponents;
    using Ical.Net.DataTypes;
    using Ical.Net.Serialization;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using Calendar = Ical.Net.Calendar;

    class Program
    {
        static void Main(string[] args)
        {            
            var request = (HttpWebRequest)WebRequest.Create("http://localhost:3000/devocionais");
            request.ContentType = "application/json";
            request.Method = "GET";
            var ano = 2022;

            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                string json = streamReader.ReadToEnd();

                var lstDevocionais = JsonConvert.DeserializeObject<List<Devocional>>(json);

                var calendar = new Calendar();
                calendar.ProductId = "mk77.com.br";

                var anoBissexto = DateTime.IsLeapYear(ano);

                var leituraDoAnoBissexto = string.Empty;

                if (anoBissexto)                
                    leituraDoAnoBissexto = lstDevocionais.Where(l => l.Mes == "fevereiro" && l.Dia == 29).Select(l => l.Leitura).First();                

                foreach (var item in lstDevocionais)
                {
                    if ((item.Mes == "fevereiro") && item.Dia == 29 && (!anoBissexto))
                        continue;
                    
                    var frequencia = new RecurrencePattern
                    {
                        Frequency = FrequencyType.Yearly,
                        Interval = 1,
                        ByMonthDay = new List<int> { item.Dia },
                        Until = new DateTime(ano, 12, 31)
                    };

                    var evento = new CalendarEvent();
                    evento.Summary = "Devocional";

                    if ((item.Mes == "fevereiro") && item.Dia == 28 && (!anoBissexto))
                        evento.Description = item.Leitura += $"; {leituraDoAnoBissexto}";
                    else
                        evento.Description = item.Leitura;

                    evento.Start = new CalDateTime(ano, DateTime.ParseExact(item.Mes, "MMMM", CultureInfo.CurrentCulture).Month, item.Dia, 6, 0, 0);
                    evento.End = new CalDateTime(ano, DateTime.ParseExact(item.Mes, "MMMM", CultureInfo.CurrentCulture).Month, item.Dia, 7, 0, 0);
                    evento.RecurrenceRules = new List<RecurrencePattern>() { frequencia };                    
                    calendar.Events.Add(evento);
                }

                var iCalSerializer = new CalendarSerializer();
                string result = iCalSerializer.SerializeToString(calendar);
                File.WriteAllBytes(@"c:\temp\BibliaEmUmAno.ics", Encoding.UTF8.GetBytes(result));
            }
        }
    }

    public class Devocional
    {
        public string Mes { get; set; }

        public int Dia { get; set; }

        public string Leitura { get; set; }
    }
}
