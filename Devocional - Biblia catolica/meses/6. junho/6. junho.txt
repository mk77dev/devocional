Junho
1 Oséias 10-12, Salmos 25
	 
2 Oséias 13-14, Isaías 1, Salmos 26
	 
3 Isaías 2-4, Salmos 27
	 
4 Isaías 5-7, Salmos 28
   
5 Isaías 8-10, Salmos 29
	 
6 Isaías 11-13, Salmos 30
	 
7 Isaías 14-16, Salmos 31
	 
8 Isaías 17-19, Salmos 32
	 
9 Isaías 20-22, Salmos 33
	 
10 Isaías 23-25, Salmos 34
	 
11 Isaías 26-28, Salmos 35
	 
12 Isaías 29-31, Salmos 36
	 
13 Isaías 32-34, Salmos 37
	 
14 Isaías 35-37, Salmos 38
	 
15 Isaías 38-39, Miquéias 1, Salmos 39
	 
16 Miquéias 2-4, Salmos 40
	 
17 Miquéias 5-7, Salmos 41
	 
18 Naum 1-3, Salmos 42
	 
19 Sofonias 1-3, Salmos 43
	 
20 Habacuque 1-3, Salmos 44
	 
21 Jeremias 1-3, Salmos 45
	 
22 Jeremias 4-6, Salmos 46
	 
23 Jeremias 7-9, Salmos 47
	 
24 Jeremias 10-12, Salmos 48
	 
25 Jeremias 13-15, Salmos 49
	 
26 Jeremias 16-18, Salmos 50
	 
27 Jeremias 19-21, Salmos 51
	 
28 Jeremias 22-24, Salmos 52
	 
29 Jeremias 25-27, Salmos 53
	 
30 Jeremias 28-30, Salmos 54